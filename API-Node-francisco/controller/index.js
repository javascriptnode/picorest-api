const deleteUsers = require('./deleteUserController.js');
const getUser = require('./getUserController.js');
const getUsers = require('./getUsersController.js');
const userPost = require('./userPostController.js');
const userPut = require('./userPutController.js');


module.exports = [deleteUsers, getUser, getUsers, userPost, userPut];