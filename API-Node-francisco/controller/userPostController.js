const Users = require('../models/users');

const express = require('express');
let route = express.Router();

route.post('/register', async (req,res)=>{

    try{
        let user = await Users.create(req.body);
        res.send(user);
    }catch(e){
        res.status(400).send({ error: 'Falha em tentar registrar' });
    }
});

module.exports = route;