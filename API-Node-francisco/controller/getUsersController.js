const Users = require('../models/users');

const express = require('express');
let route = express.Router();


route.get('/', async (req,res)=>{

    try {

        let users = await Users.find({});
        res.send(users);

    } catch (e) {

        res.status(400).send({ error: 'Falha em Localizar os usuarios' });
    }

});

module.exports = route;