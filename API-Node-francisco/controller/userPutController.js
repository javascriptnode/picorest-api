const Users = require('../models/users');

const express = require('express');
let route = express.Router();

route.put('/:id', async(req,res)=>{

    try{

        let user = await Users.findOneAndReplace({_id:req.params.id},req.body,{new : true});
        res.send(user);
        
    }  catch(e){

        res.status(400).send({ error: 'Falha em Localizar usuario' });

    }

});

module.exports = route;