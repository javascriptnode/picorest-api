const Users = require('../models/users');

const express = require('express');
let route = express.Router();

route.get('/:id', async(req,res)=>{

    try{

        let user = await Users.findById({_id:req.params.id});
        res.send(user);
        
    }  catch(e){

        res.status(400).send({ error: 'Falha em Localizar usuario' });

    }

});

module.exports = route;