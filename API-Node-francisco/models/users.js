const mongoose = require('../database/index');

const UserSchema = new mongoose.Schema({

    name: {
        type:String,
        require: true,
    },
    email: {
        type:String,
        require: true,
        unique:true,
        lowercase:true,

    },
    password: {
        type:String,
        required:true,
        select:true,

    },
    createAt: {
        type: Date,
        default: Date.now,

    },


});

const Users = mongoose.model('User', UserSchema);

module.exports = Users;