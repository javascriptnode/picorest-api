const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost:27017/API', (error)=>{
    if(error){
        console.log('Não conectou');
    }else{
        console.log('conectou!!')
    }

});

mongoose.set('useNewUrlParser', true);

mongoose.Promise = global.Promise;

module.exports = mongoose;