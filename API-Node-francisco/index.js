const express = require('express');
const bodyParser = require('body-parser');
let routeIndex = require('./controller/index');
let app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:false }));

routeIndex.forEach(e=>{

    app.use('/users',e);

});


app.listen(3000,'', ()=>{

    console.log("Servidor Rodando !!");

});

